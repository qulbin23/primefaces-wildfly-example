package beans;

import java.io.Serializable;

import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.bean.ManagedBean;

//@Named("helloworld")
@ManagedBean(name = "helloworld", eager = true)
@ViewScoped
public class HelloWorldBean implements Serializable {

   /**
	 * 
	 */
	private static final long serialVersionUID = 6676468902475753629L;

public String getMessage() {
      return "Hello World from Wakanda";
   }

}
